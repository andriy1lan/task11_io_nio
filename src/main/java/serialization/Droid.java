package serialization;
import java.io.Serializable;

public class Droid implements Serializable {

    transient int batteryCharge;
    String name;

    public Droid () {
    }

    public Droid (String name, int batteryCharge)  {
        this.name = name;
        this.batteryCharge=batteryCharge;
    }

    public void setName(String name)  {
        this.name = name;
    }
    public String getName()  {
        return name;
    }

    public void setBatteryCharge(int batteryCharge)  {
        this.batteryCharge = batteryCharge;
    }
    public int getBatteryCharge()  {
        return batteryCharge;
    }

}