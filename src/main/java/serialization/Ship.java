package serialization;

import java.util.List;
import java.util.ArrayList;
import java.io.Serializable;

public class Ship<T> implements Serializable {
    List<T> list;
    public Ship () {
        list = new ArrayList<T>();
    }

    public void putDroid(T droid) {
        list.add(droid);
    }

    public T getDroid(String name) {
        T droid = null;
        for(T d:list) {
            if (((Droid)d).getName().equals(name)) droid=d;
        }
        return droid;
    }

    public List<T> getDroids() {
        List<T> droids=this.list;
        return droids;
    }

}