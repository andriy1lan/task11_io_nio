import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.File;

public class FindComments {
    static boolean isMultiLine;
    static boolean isJavaDoc;
    static StringBuilder multi;
    static StringBuilder javadoc;

    private static String getFileExtension(File file) {
        String name = file.getName();
        int lastIndexOf = name.lastIndexOf(".");
        if (lastIndexOf == -1) {
            return "";
        }
        return name.substring(lastIndexOf+1);
    }

    public static void displayComments(String path) throws IOException {
        FileReader fr = null;
        LineNumberReader lnr = null;
        String str;
        int j;
        try {
            //if(!getFileExtension(new File("TestComments.java")).equals("java")) {System.out.println("It is not java source file"); return;}
            fr = new FileReader(path);
            //fr = new FileReader("C:/TestComments.java");
            lnr = new LineNumberReader(fr);

            // read lines till the end of the stream
            while((str = lnr.readLine())!=null) {
                j = lnr.getLineNumber();
                if (isMultiLine) {
                    if (str.contains("*/")) {
                        str=str.substring(0,str.indexOf("*/")+2);
                        multi.append(str);
                        System.out.print("Multi: ");
                        System.out.println(multi.toString());
                        isMultiLine=false;
                    }
                    else {multi.append(str);
                        multi.append("\n");}
                }

                if (isJavaDoc) {
                    if (str.contains("*/")) {
                        str=str.substring(0,str.indexOf("*/")+2);
                        javadoc.append(str);
                        System.out.print("JavaDoc: ");
                        System.out.println(javadoc.toString());
                        isJavaDoc=false;
                    }
                    else {javadoc.append(str);
                        javadoc.append("\n");}
                }

                else if (str.contains("//")) {
                    int index=0;
                    char[] array=str.toCharArray();
                    //to avoid including into inline comments such expression - "//"
                    for(int i=0; i<array.length; i++) {if (array[i]=='/' && array[i+1]=='/' && array[i+2]!='"' && array[i-1]!='"') {index=i;
                        str=str.substring(index);
                        System.out.print("Single: ");
                        System.out.println(str);
                        break;
                    }
                    }
                }

                else if (str.contains("/*") && !str.contains("/**")) {multi=new StringBuilder();
                    str=str.substring(str.indexOf("/*"));
                    multi.append(str);
                    multi.append("\n");
                    isMultiLine=true;  }

                else if (str.contains("/**") && str.contains("/**")) {javadoc=new StringBuilder();
                    str=str.substring(str.indexOf("/**"));
                    javadoc.append(str);
                    javadoc.append("\n");
                    isJavaDoc=true;  }
            }

        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            if(fr!=null)
                fr.close();
            if(lnr!=null)
                lnr.close();
        }


    }

    //1.Enter on Command Line to folder with FindComments.java file
    //2.Enter in command line - javac FindComments.java
    //3.Enter in command line - java FindComments TestComments.java -- or C:/TestComments.java etc
    public static void main(String[] args) throws IOException {
        displayComments(args[0]);
    }
}
