import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedInputStream;
import java.time.LocalTime;
import java.time.Duration;

public class InputReadings {


    public static void getStandartInputReading() {

        LocalTime lt1 = LocalTime.now();
        System.out.println(LocalTime.now());
        try {
            InputStream is = new FileInputStream("C:/BigFile.pdf");
            //BufferedInputStream bis = new BufferedInputStream(is);
            //DataInputStream in = new DataInputStream(bis);
            int r;
            int c = 0;
            while (is.available() > 0) {
                r = is.read();
                c++;
               // if (c % 100000 == 0)
               //     System.out.println(c);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(LocalTime.now());
        Duration duration1 = Duration.between(lt1, LocalTime.now());
        System.out.println("Reading (in seconds) by Standart FileInputStreamReader: " + duration1.getSeconds());

    }

    public static void getBufferedInputReading() {
        int buf = 0;
        for (int i = 0; i < 10; i++) {
            buf = (i + 1) * 1000;
            LocalTime lt2 = LocalTime.now();
            System.out.println(LocalTime.now());
            try {
                InputStream is = new FileInputStream("C:/BigFile.pdf");
                BufferedInputStream bis = new BufferedInputStream(is, buf);
                //DataInputStream in = new DataInputStream(bis);
                int r;
                int c = 0;
                while (bis.available() > 0) {
                    r = bis.read();
                    c++;
                 //   if (c % 100000 == 0)
                 //       System.out.println(c);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println(LocalTime.now());
            Duration duration2 = Duration.between(lt2, LocalTime.now());
            System.out.println("Reading (in seconds) by BufferedInputStreamReader with buffersize: " + duration2.getSeconds() + " buffersize -" + buf);
        }

    }


    public static void main(String args[]) {
        getStandartInputReading();
        getBufferedInputReading();

    }

}