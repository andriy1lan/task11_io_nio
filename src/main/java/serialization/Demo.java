package serialization;

import java.io.ObjectOutputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.util.List;

public class Demo {


    public static void main (String args[]) {


    Ship<Droid> ship=new Ship<Droid>();
    Droid d1 = new Droid("cdroid",12);
    Droid d2 = new Droid("adroid",23);
    Droid d3 = new Droid("bdroid",35);
    ship.putDroid(d1);
    ship.putDroid(d2);
    ship.putDroid(d3);

    try{
    ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("src/main/resources/shipofdroids.dat"));
    out.writeObject(ship);
    out.close();
    System.out.println("Ship of droids is serialized to file - <<shipofdroids.dat>>");
    }

    catch(IOException ie) {
    ie.printStackTrace();}

    Ship<Droid> ship1 = null;
    try{
    ObjectInputStream in = new ObjectInputStream(
    new FileInputStream("src/main/resources/shipofdroids.dat"));
    ship1 = (Ship<Droid>)in.readObject();
    in.close();
    }
    catch(IOException ie) {
    ie.printStackTrace();
    return;}
    catch(ClassNotFoundException ce) {
    ce.printStackTrace();
    return;}

    List<Droid> droids = ship1.getDroids();
    for(Droid d:droids) {
    System.out.println("Droid recovered - "+ d.getName() +" "+ d.getBatteryCharge());
    }

    }
}