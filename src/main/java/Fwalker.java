import java.util.List;
import java.util.ArrayList;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.Date;

public class Fwalker {
    long length;
    String name;
    String path;

    Fwalker(long length, String name, String path)
    {
        this.length = length;
        this.name = name;
        this.path = path;
    }

    static List <Fwalker> listed = new ArrayList <Fwalker> ();
    static int files;
    static int dirs;

    public static void walk( String path ) {
        File currentDir = new File( path );
        File[] list = currentDir.listFiles();
        if (list == null) return;
        for ( File f : list ) {
            if ( f.isDirectory() ) {
                dirs++;
                File[] filesd = f.listFiles(new FileFilter() {
                    @Override
                    public boolean accept(File f) {
                        return f.isDirectory();
                    }
                });
                File[] filesf = f.listFiles(new FileFilter() {
                    @Override
                    public boolean accept(File f) {
                        return f.isFile();
                    }
                });

                System.out.println( "Directory" + dirs + ": " + f.getName()+ "  " + f.getPath() +" "+ filesd.length+" dirs "+filesf.length+" files "+new Date(f.lastModified()));
                walk( f.getAbsolutePath() );
            }
            else {
                files++;
                Fwalker odyn = new Fwalker (f.length(), f.getName(), f.getPath());
                listed.add(odyn);
                System.out.println( "File"+files+": " + f.length() + " bytes  " + f.getName()+ "  " + f.getPath()+" "+ new Date(f.lastModified())); }
        }
    }


    public static void main(String[] args) throws IOException {
        //Fwalker.walk(args[0]);
        Fwalker.walk("C:/budget");
        System.out.println("Number of FILES: "+Fwalker.files+" -- Number of FOLDERS: "+Fwalker.dirs);
        System.out.println("Size of list with basic files info: "+ Fwalker.listed.size());
    }

}