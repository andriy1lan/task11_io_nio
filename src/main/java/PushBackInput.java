import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.DataInputStream;
import java.util.Stack;

public class PushBackInput {

    public PushBackInput(InputStream inputstream)  throws IOException{
        this.inputstream = inputstream;
        b = new byte[inputstream.available()];
        //String str = "Java language is the most popular programming language";
        DataInputStream dis = new DataInputStream(this.inputstream);
        dis.readFully(b);
        dis.close();
        //System.out.println(b.length);
    }

    String str = "Java language is the most popular programming language";
    InputStream inputstream;
    byte[] b;
    Stack<Byte> buffer1=new Stack<Byte>();
    //position in initial stream(byte array)
    int pos=0;
    //the size of pushback buffer
    int buflimit=6;

    public int available() throws IOException {
        return b.length - pos;
    }

    public int read() throws IOException {
        if (buffer1.size() > 0) {
            return buffer1.pop();
        }
        else if (b.length > 0 && pos<b.length) {
            return b[pos++];
        }
        else return -1;
    }

    public void unread(int b) throws IOException {
        if (buffer1.size() < buflimit) {
            buffer1.push((byte)b);
        }
    }

    public void unread(byte[] b,int off,int len) throws IOException {
        if (buffer1.size() < buflimit) {
            int left=buflimit-buffer1.size();
            if (left > len) left = len;
            //System.out.println(left);
            for(int i=off+left-1; i>=off; i--) {
               // System.out.println((char)b[i]+"**");
                buffer1.push((byte)b[i]);
                if (buffer1.size() == buflimit) break;
            }
        }
    }

    public int read (byte[] bb, int off, int len) throws IOException {
        int length = 0;
        if (b.length > 0 && off<(b.length-buffer1.size()) && len<=(b.length - off-buffer1.size())) {
           // System.out.println(buffer1.size()+"?");
            for(int i = off; i<off+len; i++) {
                if (i>=bb.length) break;
                if (buffer1.size()>0) { bb[i] = (byte)this.buffer1.pop(); length++;}
                else if(pos<b.length) {
                    bb[i] = (byte)this.read(); length++;}
            }
        }
        return length;
    }

    public static void main(String arg[]) throws Exception
    {
        String str = "Java language is the most popular programming language";
        byte[] bytes = str.getBytes();
        InputStream myInputStream = new ByteArrayInputStream(bytes);
        PushBackInput pi=new PushBackInput(myInputStream);
        //System.out.print(pi.available()+"av");
        //Where to read from pushbackstream
        byte[] buf = new byte[9];
        //current char(byte) in stream
        System.out.println((char)pi.b[pi.pos]);
        String str1 = "Java language is the most popular programming language";
        byte[] b1 = str1.getBytes();
        //read to pushback buffer, and remaining - to pushbackstream from outer byte array
        pi.unread(b1,5,11);
        //size of pushback (stack) buffer - dynamically changed
        System.out.println(pi.buffer1.size());
        //length of read from pushbackstream to outerbuffer - not 12 - but 9 as size of the outer buffer
        int l=pi.read(buf,0,12);
        System.out.println(l);
        for (int i = 0; i<buf.length; i++) System.out.print((char)buf[i]);
    }
}
